import React from "react";
import img from '../img/image-victor.jpg';
import '../css/TarjetaPerfil.css'


function TarjetaPerfil( {name,age,country,followers,likes,photos} ){
  return(
    <section className="container-tarjeta">
      <img src={img} className="porfile-photo"></img>
      <div className="container-person">
        <div className="name">
          <h2>{name}</h2>
          <p>{age}</p>
        </div>
        <p>{country}</p>
      </div>
      <div className="container-data">
        <div className="stats">
          <h2>{followers}</h2>
          <p>Followers</p>
        </div>
        <div className="stats">
          <h2>{likes}</h2>
          <p>Likes</p>
        </div>
        <div className="stats">
          <h2>{photos}</h2>
          <p>Photos</p>
        </div>
      </div>
    </section>      
  )
};


export default TarjetaPerfil;