import TarjetaPerfil from './componentes/TarjetaPerfil';
import './App.css';
import data from  './usersData.json';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <TarjetaPerfil 
          name={data[0].name}
          age={data[0].age}
          country={data[0].country}
          followers={data[0].followers}
          likes={data[0].likes}
          photos={data[0].photos}
        />

      </header>
    </div>
  );
}

export default App;
